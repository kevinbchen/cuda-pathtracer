# Optimizations

These are the optimizatons (and attempts at optimizatons) used to try and speed 
up the pathtracer. Note that `i/s` is iterations per second. The exact numbers
are not terribly important because of differences in machines; rather the relative
increases/decreases are significant.


#### BIH structure
- makes large numbers of primitives possible
- BIH tree seems to work best when leaves only contain ~1 primitive
    - node traversals are apparently MUCH cheaper than triangle intersections
    - for example, going from a max depth of 30 to 50 actually slightly
      improves speed (marginal, but noticeable)
- BIH struct was initially implemented extremely naively (sort of fixed)
    - still using 2 children pointers; original paper uses 1 pointer (
      by grouping children together presumably)
- tried moving bih data into texture memory, did not work
    - probably due to how bih tree is flattened (currently preorder)
        - did also try to group sibling nodes together, but made it slightly
          slower

#### BVH structure
- initial implementaton was comparable to BIH
- however, putting BVH data into texture memory works well (9.9 i/s to 
  12.9 i/s)
- optimized BVH structure by fixing some major oversights (5 i/s to 8.5 i/s
  for new reference scene; up to 19.7 i/s for old scene)
    - skip node traversal if already found an intersection closer than node.
    - before adding child to stack, check if it is "behind" the ray for the 
      current axis; this was very helpful for rays that started within the
      BVH (ie. all non-primary rays)

#### Texture memory instead of global memory
- moved primitives, BVH data into texture memory
    - 8.5 i/s to 12 i/s for just the primitives
- some strange behavior (a repeated texture lookups actually faster than
  using data from a populated primitive struct for color/emission...)
    - struct is in local memory perhaps? (not completely sure if structs
      can be stored in registers, esp. if reference is used).

#### Micro-optimizations
- normals are stored with primitives
- was able to avoid using a stack for a single path (by keeping separate
  color accumulations)
- optimized main pathtracing codepath (8.5 i/s to 9.9) through rearranging
  some function calls

#### Decoupled materials from primitive structure
- separated materials from primitive structure (primitives have an index into
 the material array)
    - resulted in ~25% speedup on lab machines (16 i/s to 20 i/s)
    - however, made no noticeable difference on personal machine...
    - in addition, shared memory was not noticeably faster than global memory
     for storing the materials array

## Unsuccessful optimizations
- shared memory
    - while not possible for large numbers of primitives, it actually didn't 
     seem to help for small numbers either (~3 spheres)...
    - also doesn't help much for materials array
- tried to do some clever indexing for BIH to avoid code to swap variables
    - actually slower (probably because of the out of order memory access?)

## Additional optimizations to try
- parallelize by ray instead of by pixel (use CUDAPP to compact stream)
    -this should help with the issue of idle threads in warps
- stratified sampling (won't speed in terms of i/s, but should reduce noise)
- implement SAH (surface area heuristic) to pick splitting planes for BIH
    - started this, but bugs in implementation (also really slow..)
- implement different spatial structure (kd-tree?)
    - kd-tree might be harder since a primitive can be in more than one node
     (realized this after starting to work on it)
- look at register usage (not completely sure how registers work for structs)
