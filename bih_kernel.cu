#ifndef _BIH_KERNEL_CU_
#define _BIH_KERNEL_CU_

#include <curand_kernel.h>
#include "cutil_math.h"
#include "primitives.h"
#include "bih.h"

__constant__ float3 maxBound;
__constant__ float3 minBound;


__device__ int getNearestIntersection(
    Ray &ray, BIHNode nodes[], Primitive primitives[], float &t, Primitive &primitive, int &numTraversals) {
	/*float3 invDir;
	invDir.x = 1.0f/ray.d.x;
	invDir.y = 1.0f/ray.d.y;
	invDir.z = 1.0f/ray.d.z;*/

	float tmin = 0.0f;
	float tmax = 100000.0f;

    // compute intersection of ray with all six bbox planes
    float3 invR = make_float3(1.0f) / ray.d;
    float3 tbot = invR * (minBound - ray.o);
    float3 ttop = invR * (maxBound - ray.o);

    // re-order intersections to find smallest and largest on each axis
    float3 tmin3 = fminf(ttop, tbot);
    float3 tmax3 = fmaxf(ttop, tbot);

    // find the largest tmin and the smallest tmax
    float largest_tmin = fmaxf(fmaxf(tmin3.x, tmin3.y), tmin3.z);
    float smallest_tmax = fminf(fminf(tmax3.x, tmax3.y), tmax3.z);

    tmin = largest_tmin;
    tmax = smallest_tmax;
    if (tmax <= tmin) return -1;

    //return &primitives[0];

	/*stack<BIHNode*> nodes;
	stack<float> tmins;

	BIHNode* node = rootNode;

	while (tmin < 10000.0f) {
		while (node && node->type != BIHNode::LEAF) {
			int axis = node->type;
			float leftPlane = (node->clip[0]-ray.p.cell[axis])/ray.d.cell[axis];
			float rightPlane = (node->clip[1]-ray.p.cell[axis])/ray.d.cell[axis];

			BIHNode *nearNode = NULL;
			BIHNode *farNode = NULL;
			float nearPlane, farPlane;

			if (ray.d.cell[axis] < 0.0f) {
				nearPlane = rightPlane;
				farPlane = leftPlane;
				nearNode = node->right;
				farNode = node->left;
			}
			else {
				nearPlane = leftPlane;
				farPlane = rightPlane;
				nearNode = node->left;
				farNode = node->right;
			}

			if (farPlane > tmax) {
				if (nearPlane > tmin) {
					tmax = fminf(tmax,nearPlane);
					node = nearNode;
				}
				else {
					if (nodes.empty()) {
						return false;
					}
					else {
						node = nodes.top();
						nodes.pop();

						tmin = tmins.top();
						tmins.pop();
					}
				}
			}
			else if (nearPlane < tmin) {
				tmin = fmaxf(tmin,farPlane);
				node = farNode;
			}
			else {
				tmin = fminf(tmin,farPlane);
				nodes.push(farNode);
				tmins.push(tmin);
				node = nearNode;
			}
		}

		if (node) {
			return true;
			float distance = -1.0f;
			Primitive* primitive = NULL;

			intersection.t = -1;
			for (int i=0; i<node->objects.size(); i++) {
				Primitive* p = node->objects[i];
				Intersection tempIntersection;
				if (p->intersect(ray,tempIntersection)) {
					if (intersection.t == -1 || tempIntersection.t < intersection.t) {
						intersection = tempIntersection;
						primitive = p;
					}
				}
			}
			if (primitive != NULL) {
				return true;
			}
		}

		if (nodes.empty()) {
			return false;
		}
		else {
			node = nodes.top();
			nodes.pop();

			tmin = tmins.top();
			tmins.pop();
		}
	}

	return false;*/
	//int numPrimitives = 0;

	//float distance = tmax;
	//t = tmax;

    BIHInfo BIHStack[51];
	//stack<BIHInfo> BIHStack;
	//BIHStack.push(BIHInfo(rootNode,tmin,tmax));

    int primitiveIndex = -1;
	BIHNode node = nodes[0];

    int index = -1;
	bool useStack = false; //don't use stack for first one
	while (true) {        
		numTraversals++;        
		if (useStack) {
			if (index < 0) break;
			node = nodes[BIHStack[index].node];
			tmin = BIHStack[index].tmin;
			tmax = BIHStack[index].tmax;
			index--;//BIHStack.pop();
		}        
        useStack = true;
		if (t <= tmin) continue;	

		//if (node == NULL) continue;
        int axis = node.children[0] & 0x3;
		if (axis != BIHNode::LEAF) {
			//int axis = node.type;
            float rayo = ((float *)&ray.o)[axis];
            float rayd = ((float *)&ray.d)[axis];
            float invDir = 1.0f / rayd;

            //uint sign = signbit(rayd);

			float nearPlane = (node.clip[0]-rayo)*invDir;///ray.d.cell[axis];
			float farPlane = (node.clip[1]-rayo)*invDir;///ray.d.cell[axis];

			int nearNode = node.children[0] >> 2;//&nodes[node->left];
			int farNode = node.children[1];//&nodes[node->right];

			if (rayd < 0.0f) {                
				float tempPlane = nearPlane;
				nearPlane = farPlane;
				farPlane = tempPlane;

				int tempNode = nearNode;
				nearNode = farNode;
				farNode = tempNode;
			}

			if (tmin > nearPlane && tmax < farPlane) { // empty
				//continue;
			} else if (tmin > nearPlane) { // far node only
				if (farNode != -1) {
					//tmin = fmaxf(tmin,farPlane);
					//BIHStack.push(BIHInfo(farNode,fmaxf(tmin,farPlane),tmax));
					node = nodes[farNode];
					tmin = fmaxf(tmin, farPlane);
					useStack = false;
					//continue;
				}
			} else if (tmax < farPlane) { // near node only
				if (nearNode != -1) {
					//tmax = fminf(tmax,nearPlane);
					//BIHStack.push(BIHInfo(nearNode,tmin,fminf(tmax,nearPlane)));
					node = nodes[nearNode];
					tmax = fminf(tmax, nearPlane);
					useStack = false;
				}
			} else { // near to far (both)
				if (farNode != -1) {
					if (nearNode != -1) {
                        index++;
						BIHStack[index].node = farNode;
                        BIHStack[index].tmin = farPlane;
                        BIHStack[index].tmax = tmax; //= BIHInfo(farNode,farPlane,tmax);
					} else {
						useStack = false;
						node = nodes[farNode];
						tmin = farPlane;
					}
				}
				if (nearNode != -1) {
					//BIHStack.push(BIHInfo(nearNode,tmin,nearPlane));
					useStack = false;
					node = nodes[nearNode];
					tmax = nearPlane;
				}
			}
		} else {
            //return 0;
        	//intersection.t = -1;
			//for (int i=0; i<node->objects.size(); i++) {
            //Intersection tempIntersection;
            Primitive tempPrimitive;
			for (int i = node.children[0] >> 2; i <= node.children[1]; i++) {
				//numPrimitives++;
                float4 x = tex1Dfetch(primitivesTex, i * 7 + 0);
                tempPrimitive.type = *(int*)&x.x;
                tempPrimitive.m = *(int*)&x.y;
                tempPrimitive.p1 = make_float3(tex1Dfetch(primitivesTex, i * 7 + 1));
                tempPrimitive.p2 = make_float3(tex1Dfetch(primitivesTex, i * 7 + 2));
                tempPrimitive.p3 = make_float3(tex1Dfetch(primitivesTex, i * 7 + 3));
                /*tempPrimitive.c = make_float3(tex1Dfetch(primitivesTex, i * 7 + 4));
                tempPrimitive.e = make_float3(tex1Dfetch(primitivesTex, i * 7 + 5));*/
                

                float tempT = primitiveIntersect(ray, tempPrimitive); //primitives[i]);
                if (tempT > 0 && tempT < t) {
					t = tempT;
					primitiveIndex = i;
                    primitive = tempPrimitive;
					//distance = in.t;			
				}
			}
			/*if (primitive != NULL) {
				//return true;
			}*/
		}
	}
	//in.numTraversals = numTraversals;
	//in.numPrimitives = numPrimitives;
	return primitiveIndex;
}

#endif
