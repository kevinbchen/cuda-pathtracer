#include "bvh.h"

BVH::BVH(vector<Primitive> *primitives, float3 minb, float3 maxb) {
	this->primitives = primitives;
	minBound = minb;
	maxBound = maxb;
}

BVH::~BVH(void) {
}


float BVH::primitiveCenter(Primitive &p, int axis) {
	if (p.type == TRIANGLE) {
        return ((float *) &((p.p1 + p.p2 + p.p3)/3.0f))[axis];
	} else if (p.type == SPHERE) {
        return ((float *) &p.p1)[axis];
	}
}

float BVH::primitiveMax(Primitive &p, int axis) {
	if (p.type == TRIANGLE) {
        return ((float *) &fmaxf(fmaxf(p.p1, p.p2), p.p3))[axis];
	} else if (p.type == SPHERE) {
        return ((float *) &(p.p1 + make_float3(p.p2.x)))[axis];
	}
}
float3 BVH::primitiveMax(Primitive &p) {
	if (p.type == TRIANGLE) {
        return fmaxf(fmaxf(p.p1, p.p2), p.p3);
	} else if (p.type == SPHERE) {
        return p.p1 + make_float3(p.p2.x);
	}
}


float BVH::primitiveMin(Primitive &p, int axis) {
	if (p.type == TRIANGLE) {
        return ((float *) &fminf(fminf(p.p1, p.p2), p.p3))[axis];       
    } else if (p.type == SPHERE) {
        return ((float *) &(p.p1 - make_float3(p.p2.x)))[axis];
	}
}

float3 BVH::primitiveMin(Primitive &p) {
	if (p.type == TRIANGLE) {
        return fminf(fminf(p.p1, p.p2), p.p3);       
    } else if (p.type == SPHERE) {
        return p.p1 - make_float3(p.p2.x);
	}
}


int BVH::subdivide(int l, int r, float3 &minBound, float3 &maxBound, int axis, int depth) {
    if (r < l) {
        printf("no nodes\n");
        return -1;
    }
	float3 diff = maxBound-minBound;
	axis = (axis + 1) % 3;/*BVHNode::Z;
	if (diff.x > diff.y) {
		if (diff.x > diff.z) {
			axis = BVHNode::X;
		}
	}
	else {
		if (diff.y > diff.z) {
			axis = BVHNode::Y;
		}
	}*/

	if (r-l < 10 || depth >= 20) {// || diff.x + diff.y + diff.z <= 0.1) { //leaf node;        
        //if (depth >= 30) printf("depth %d %d %d\n", depth, r-l);
		//printf ("leaf (%d %d): ",l,r);
		BVHNode node;
		//node.type = BVHNode::LEAF;
		node.children[0] = (l << 2) | BVHNode::LEAF;
		node.children[1] = r;
        node.minB = minBound;
        node.maxB = maxBound;
        if (r-l > 1000) {
            printf("%f %f %f | %f %f %f\n", minBound.x, minBound.y, minBound.z,
            maxBound.x, maxBound.y, maxBound.z);
        }
        nodes.push_back(node);
		/*for (int i=l; i<=r; i++) {
			node->objects.push_back((*primitives)[i]);
			//cout << "object ";
		}*/

		//cout << "\n";
		return nodes.size() - 1;
	}
	else {
		BVHNode node;
		//node.type = axis;
		node.children[0] = ((-1) << 2) | axis;
		node.children[1] = -1;
        node.minB = minBound;
        node.maxB = maxBound;
        nodes.push_back(node);
        int nodeIndex = nodes.size()-1;
		//node->clip[0] = 0;
		//node->clip[1] = 0;

        int i;
		int j;
		float lclip;
		float rclip;
        float minB;
        float maxB;
        for (int k = 0; k < 3; k++) {
            i = l;
		    j = r;

            minB = ((float *) &minBound)[axis];
            maxB = ((float *) &maxBound)[axis];

            /*float plane = 0.0f;
            for (int a = l; a <= r; a++) {
                plane += primitiveCenter((*primitives)[a], axis) / (r - l + 1);
            }
            plane = ((maxB+minB) * 0.5) * 0.5 + plane * 0.5;*/
            float plane = (maxB+minB) * 0.5;

		    lclip = minB - 1;
		    rclip = maxB + 1;
		    while (i < j) {
			    while (primitiveCenter((*primitives)[i],axis) <= plane) {
				    float temp = primitiveMax((*primitives)[i],axis);
				    if (temp > lclip) {
					    lclip = temp;
				    }
				    i++;
				    if (i > r) break;
			    }
			    while (primitiveCenter((*primitives)[j],axis) > plane) {
				    float temp = primitiveMin((*primitives)[j],axis);
				    if (temp < rclip) {
					    rclip = temp;
				    }
				    j--;
				    if (j < l) break;
			    }
			    if (i < j) {
				    std::swap((*primitives)[i],(*primitives)[j]);
				    /*Primitive *p = primitives[i];
				    primitives[i] = primitives[j];
				    primitives[j] = p;*/
				    //i++;
				    //j--;
			    }
		    }
            if (l <= j && i <= r) break;
            axis = (axis + 1) % 3;
        }

		if (lclip >= minB) {
            float3 newMaxBound = minBound;
            float3 newMinBound = maxBound;
            for (int k = l; k <= j; k++) {
                newMaxBound = fmaxf(newMaxBound, primitiveMax((*primitives)[k]));
                newMinBound = fminf(newMinBound, primitiveMin((*primitives)[k]));
            }
			node.children[0] = (subdivide(l,j,newMinBound,newMaxBound,axis,depth+1) << 2) | axis;
		}
		if (rclip <= maxB) {
            float3 newMaxBound = minBound;
            float3 newMinBound = maxBound;
            for (int k = i; k <= r; k++) {
                newMaxBound = fmaxf(newMaxBound, primitiveMax((*primitives)[k]));
                newMinBound = fminf(newMinBound, primitiveMin((*primitives)[k]));
            }
			node.children[1] = subdivide(i,r,newMinBound,newMaxBound,axis,depth+1);
		}
        nodes[nodeIndex] = node;
		//printf("\n depth: %d axis: %d  clip: %f %f",depth,axis,node->clip[0],node->clip[1]);
        return nodeIndex;
	}
}


void BVH::build() {
	printf("Building BVH tree...");

	float3 diff = maxBound-minBound;
	int axis = BVHNode::Z;
	if (diff.x > diff.y) {
		if (diff.x > diff.z) {
			axis = BVHNode::X;
		}
	}
	else {
		if (diff.y > diff.z) {
			axis = BVHNode::Y;
		}
	}

	//rootNode = subdivide(0,primitives->size()-1,minBound,maxBound,axis,0);
    subdivide(0,primitives->size()-1,minBound,maxBound,axis,0);

	printf("Done:\n");

	int numNodes = 0;
	int numLeafs = 0;
	int numEmpty = 0;
	int numPrimitives = 0;
    int maxPrimitivesPerLeaf = 0;
	getInfo(0, numNodes, numLeafs, numEmpty, numPrimitives, maxPrimitivesPerLeaf);
	printf("  Regular Nodes: %d\n  Empty Nodes: %d\n  Leaf Nodes: %d\n  Primitives: %d\n  Max Prims Per Node: %d\n\n",
        numNodes, numEmpty, numLeafs, numPrimitives, maxPrimitivesPerLeaf);

}

void BVH::getInfo(int index, int &numNodes, int &numLeafs, int &numEmpty, int &numPrimitives,
        int &maxPrimitivesPerLeaf) {
	if (index == -1) {
		numEmpty++;
	} else if ((nodes[index].children[0] & 0x3) == BVHNode::LEAF) {
        int n = nodes[index].children[1] - (nodes[index].children[0] >> 2) + 1;
        maxPrimitivesPerLeaf = max(maxPrimitivesPerLeaf, n);
		numPrimitives += n;
		numLeafs++;
	} else {
		numNodes++;
		getInfo(nodes[index].children[0] >> 2,numNodes,numLeafs,numEmpty,numPrimitives,maxPrimitivesPerLeaf);
		getInfo(nodes[index].children[1],numNodes,numLeafs,numEmpty,numPrimitives,maxPrimitivesPerLeaf);
	}

}
