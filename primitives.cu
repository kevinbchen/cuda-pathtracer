#ifndef _PRIMITIVES_CU_
#define _PRIMITIVES_CU_

#include "primitives.h"

__device__ __forceinline__ float sphereIntersect(Ray &ray, float3 p, float r) {
    float3 v = ray.o - p;
    float b = dot(v, ray.d);
    float c = dot(v, v) - r*r;
    float d = b*b - c;
    if (d < 0.0f) {
        return -1;
    } else {
        d = sqrtf(d);
        //float t1 = fminf(fmaxf(0, -b-d), fmaxf(0, -b+d));
        float t1 = -b-d;
        if (t1 < 0.0) {
            t1 = -b+d;
        }
        //float t2 = -b+d;

        return t1;

        /*if (t2 > 0.0f) {
            if (t1 < 0.0f) t1 = t2;
        } else {
            return false;
        }
        in.t = t1;
        //in.p = ray.o + ray.d*t1;
        //in.n = normalize(in.p - p);
        //in.primitive = this;
        return true;*/
    }
}

__device__ __forceinline__ float3 sphereRandomPoint(float4 sphere, curandState *state) {
    float3 n = normalize(make_float3(
            curand_uniform(state),
            curand_uniform(state),
            curand_uniform(state)));
    float3 p = make_float3(sphere);

    return p + n * sphere.w;
}
__device__ __forceinline__ float triangleIntersect(Ray &ray, float3 p1, float3 p2, float3 p3) {
    float3 edge1 = p2 - p1;
    float3 edge2 = p1 - p3;
    float3 edgeCross = cross(edge1, edge2);

    float3 tvec = p1 - ray.o;
    float3 pvec = cross(edge2, tvec);
    float V = 1.0f / dot(edgeCross, ray.d);
    float A = V * dot(pvec, ray.d);
    if (A >= 0.0f) {
        float3 qvec = cross(edge1, tvec);
        float B = V * dot(qvec, ray.d);
        if (B >= 0.0f && (A+B) <= 1.0f) {
            float VA = dot(edgeCross, tvec);
            float T = VA*V;
            return T;
            /*if (T >= 0) {
                in.t = T;
                //in.p = ray.o + ray.d*in.t;
                //in.n = -normalize(edgeCross);
                //rUVCoords[0] = 1.0f - (A+B);
                //rUVCoords[1] = A;
                return T;
            }*/
        }
    }
    return -1;
}

__device__ __inline__ float primitiveIntersect(Ray &ray, Primitive &p) {
    switch (p.type) {
        case SPHERE:
            return sphereIntersect(ray, p.p1, p.p2.x);
        case TRIANGLE:
            return triangleIntersect(ray, p.p1, p.p2, p.p3);
        default:
            return false;
    }
}

__device__ __inline__ float3 primitiveNormal(Primitive &p, float3 x) {
    switch (p.type) {
        case SPHERE:
            return normalize(x - p.p1);
        case TRIANGLE:
            float3 v0 = p.p2 - p.p1, v1 = p.p3 - p.p1, v2 = x - p.p1;
            float d00 = dot(v0, v0);
            float d01 = dot(v0, v1);
            float d11 = dot(v1, v1);
            float d20 = dot(v2, v0);
            float d21 = dot(v2, v1);
            float invDenom  = 1.0f / (d00 * d11 - d01 * d01);
            float v = (d11 * d20 - d01 * d21) * invDenom;
            float w = (d00 * d21 - d01 * d20) * invDenom;
            float u = 1.0f - v - w;

            return normalize(u*p.n1 + v*p.n2 + w*p.n3);
    }
}


#endif
