/*
 * Lab 6 - Volume Rendering Fractals
 */

#ifndef _TRACER_KERNEL_CU_
#define _TRACER_KERNEL_CU_

#include "tracer_kernel.h"
#include "primitives.cu"


// number of iterations for julia distance
#define N 20
#define THRESHOLD 0.00001f
#define MAX_DEPTH 10
#define GAMMA 1/2.2

/* Volume texture declaration */
texture<float, 1, cudaReadModeElementType> tex;
texture<float4, 1, cudaReadModeElementType> primitivesTex;
texture<float4, 1, cudaReadModeElementType> materialsTex;
texture<float4, 1, cudaReadModeElementType> bihTex;
texture<float4, 1, cudaReadModeElementType> nodesTex;

#include "bih_kernel.cu"
#include "bvh_kernel.cu"

__constant__ float3x4 c_invViewMatrix;  // inverse view matrix

/* Need to write host code to set these */
__constant__ float4 c_juliaC; // julia set constant
__constant__ float4 c_juliaPlane; // plane eqn of 3D slice

__constant__ CameraInfo c_cameraInfo;
__constant__ bool c_debug;

extern __shared__ Material array[];

// transform vector by matrix (no translation)
__device__
float3 mul(const float3x4 &M, const float3 &v)
{
    float3 r;
    r.x = dot(v, make_float3(M.m[0]));
    r.y = dot(v, make_float3(M.m[1]));
    r.z = dot(v, make_float3(M.m[2]));
    return r;
}

// transform vector by matrix with translation
__device__
float4 mul(const float3x4 &M, const float4 &v)
{
    float4 r;
    r.x = dot(v, M.m[0]);
    r.y = dot(v, M.m[1]);
    r.z = dot(v, M.m[2]);
    r.w = 1.0f;
    return r;
}

// color conversion
__device__ uint rgbFloatToInt(float3 rgb) {
    rgb.x = powf(__saturatef(rgb.x), GAMMA);   // clamp to [0.0, 1.0]
    rgb.y = powf(__saturatef(rgb.y), GAMMA);
    rgb.z = powf(__saturatef(rgb.z), GAMMA);
    return (uint(255)<<24) | (uint(rgb.z*255)<<16) | (uint(rgb.y*255)<<8) | uint(rgb.x*255);
}

// perform setup
__global__ void
d_setup(uint imageW, uint imageH, curandState *states)
{
    uint x = __umul24(blockIdx.x, blockDim.x) + threadIdx.x;
    uint y = __umul24(blockIdx.y, blockDim.y) + threadIdx.y;
    uint i = __umul24(y, imageW) + x;

    if (x >= imageW || y >= imageH) return;

    curand_init(0, i, 0, &states[i]);
}

extern "C" void kernelSetup(dim3 gridSize, dim3 blockSize, 
        uint imageW, uint imageH, curandState *states) {
    d_setup<<< gridSize, blockSize >>>(imageW, imageH, states);
}

__device__ float3 getRandomVector(float3 n, curandState *state) {
	float n1 = curand_uniform(state);
	float n2 = curand_uniform(state);
    float theta=2.0*CUDART_PI_F*n1, phi=acos(1.0-n2);

    float3 w=n;
    float3 t=w;
    if (fabs(t.x)<=fabs(t.y) && fabs(t.x)<=fabs(t.z))
        t.x=1.0;
    else if (fabs(t.y)<=fabs(t.x) && fabs(t.y)<=fabs(t.z))
        t.y=1.0;
    else
        t.z=1.0;

	float3 u = normalize(cross(t, w));
	float3 v = cross(w, u);
    float3 dir = normalize(
            u*cosf(theta)*sinf(phi)+v*sinf(theta)*sinf(phi)+w*cosf(phi));
    return dir;
}

__device__ float3 test(curandState *state) {
    return getRandomVector(make_float3(0,1,0), state);
}

__device__ float3 traceRay(Ray &ray, Primitive primitives[], int numPrimitives, 
        Material materials[], BVHNode nodes[], 
        curandState *state) {
    float3 color = make_float3(1, 1, 1);

    int depth = 0;
    do {
        Primitive prim;
        float t = CUDART_INF_F;
        int numTraversals = 0;
        float c_reflect = 0.0;
        float c_refract = 0.0;
        float c_gloss = 0.0;
        float3 n;// = in.n;
        float3 p;// = in.p;

        if (ray.d.y < 0.0) {
            t = (-ray.o.y) / ray.d.y;
            n = make_float3(0, 1, 0);
        }

        int index = getNearestIntersection(ray, nodes, primitives, t, prim, numTraversals);
        if (c_debug) {
            float f = numTraversals/100.0;
            if (f < 0.33) {
                return make_float3(0, 0, f * 3);
            } else if (f < 0.66) {
                return make_float3(0, f * 3 - 1, (0.66-f) * 3);
            } else {
                return make_float3(f * 3 - 2, (1-f) * 3, 0);
            }
        }


        p = ray.o + ray.d*t;
        if (index == -1) {
            if (ray.d.y > 0.0 || ray.o.y < 0.0) {
                float f = max(0.0, (1.0 - ray.d.y) + 0.3) / 1;
                float d = 0.5 * (1 - ray.d.z);
                //color = make_float3(0);
                return color * f*make_float3(0.6+d/6, 0.7+d/12, 0.9-d/8);
            } else {
                //c_reflect = 0.5;
                c_gloss = 0.1;
                //color *= make_float3(0.9);
                float d = 0.5;
                if (((int)floorf(d * p.x) + (int)floorf(d * p.z)) % 2 == 0) {
                    color *= make_float3(0.2);
                    c_reflect = 0.3;
                } else {
                    color *= make_float3(0.9);
                    //c_reflect = 0.05;
                }
                
                /*float d = 0.5;
                float fx = fabs(d*p.x - floorf(d*p.x));
                float fz = fabs(d*p.z - floorf(d*p.z));
                if (fx < 0.05 || fz < 0.05) {
                    color *= make_float3(0.05);
                } else {
                    color *= make_float3(0.9);//make_float3(1.0, 0.4, 0.1);
                }*/
                //emission = make_float3(0);
            }
        } else {
            Material m = array[prim.m];


            prim.n1 = make_float3(tex1Dfetch(primitivesTex, index * 7 + 4));
            prim.n2 = make_float3(tex1Dfetch(primitivesTex, index * 7 + 5));
            prim.n3 = make_float3(tex1Dfetch(primitivesTex, index * 7 + 6));

            n = primitiveNormal(prim, p);
            if ((m.emission.x > 0 || m.emission.y > 0 || m.emission.z > 0)) {// && dot(ray.d, n) < 0) {
                return color * m.emission;
            }

            c_reflect = m.reflect;
            c_refract = m.refract;
            color *= m.color; //make_float3(tex1Dfetch(primitivesTex, index * 9 + 7));// prim->c;
            //emission = m.emission; // make_float3(tex1Dfetch(primitivesTex, index * 9 + 8));//prim->e;
        }

        if (c_refract > 0.0 && curand_uniform(state) < c_refract) {
            float r = 0.66f;
            float ddotn = dot(ray.d, n);
            if (ddotn > 0.0f) {
                n = -n;
                ddotn = -ddotn;
                r = 1/r;
            }

            float B = r*r*(1 - ddotn*ddotn);
            if (B > 1 || curand_uniform(state) < powf(1+ddotn, 2)) {
                //total internal reflection
                ray.o = p + n*THRESHOLD;
                ray.d = reflect(ray.d, n);
            } else {
                ray.o = p - n*THRESHOLD;
                float f = (-ddotn*r - sqrtf(1-B));
                ray.d = normalize(n*f + ray.d*r);
            }
        } else if (c_reflect > 0.0 && curand_uniform(state) < c_reflect) {
            ray.o = p + n*THRESHOLD;
            ray.d = reflect(ray.d, n);
            float3 dx = cross(ray.d, make_float3(0,1,0));
            float3 dy = cross(ray.d, dx);
            ray.d += c_gloss * (curand_uniform(state)*2 - 1) * dx;
            ray.d += c_gloss * (curand_uniform(state)*2 - 1) * dy;
            ray.d = normalize(ray.d);
        } else {
            ray.o = p + n*THRESHOLD;
            ray.d = getRandomVector(n, state);
        }

        //color += traceRay(newRay, depth + 1, state);
        depth++;

    } while (depth < MAX_DEPTH);
    
    return make_float3(0);
}

// perform volume rendering
__global__ void d_render(uint *d_output, float3 *colors, 
        uint imageW, uint imageH, 
        Primitive primitives[], int numPrimitives,
        Material materials[], int numMaterials,
        BVHNode nodes[],
        int iteration, curandState *states) {

    uint x = __umul24(blockIdx.x, blockDim.x) + threadIdx.x;
    uint y = __umul24(blockIdx.y, blockDim.y) + threadIdx.y;
    //uint x = __umul24(threadIdx.x, gridDim.x) + blockIdx.x;
    //uint y = __umul24(threadIdx.y, gridDim.y) + blockIdx.y;
    uint i = __umul24(y, imageW) + x;

    if (x >= imageW || y >= imageH) return;

    // copy primitives array to shared memory
    uint tid = threadIdx.x + __umul24(threadIdx.y, blockDim.x);
    if (tid < numMaterials) {
        array[tid] = materials[tid];
    }
    __syncthreads();

    curandState state = states[i];

    float u = (x / (float) imageW)*2.0f-1.0f;
    float v = (y / (float) imageH)*2.0f-1.0f;

    // calculate eye ray in world space
    Ray eyeRay;
    eyeRay.o = make_float3(mul(c_invViewMatrix, make_float4(0.0f, 0.0f, 0.0f, 1.0f)));
    eyeRay.d = normalize(make_float3(u, v, -2.0f));
    eyeRay.d = mul(c_invViewMatrix, eyeRay.d);

    float3 focalPoint = eyeRay.o + eyeRay.d * c_cameraInfo.focalLength;
    float3 dir = mul(c_invViewMatrix, make_float3(0, 0, -1));
    float3 dx = cross(dir, make_float3(0, 1, 0));
    float3 dy = cross(dx, dir);

    eyeRay.o += c_cameraInfo.apertureSize * (curand_uniform(&state)*2 - 1) * dx;
    eyeRay.o += c_cameraInfo.apertureSize * (curand_uniform(&state)*2 - 1) * dy;
    eyeRay.d = normalize(focalPoint - eyeRay.o);

    float3 color = traceRay(eyeRay, primitives, numPrimitives, materials, nodes, &state);
    float f = 1.0 / iteration;
    colors[i] = (1 - f)*colors[i] + f*color;

    d_output[i] = rgbFloatToInt(colors[i]);
    states[i] = state;
}

// perform volume rendering
extern "C" void kernelRender(dim3 gridSize, dim3 blockSize, uint sharedMemSize,
        uint *d_output, float3 *colors, 
        uint imageW, uint imageH, 
        Primitive primitives[], int numPrimitives,
        Material materials[], int numMaterials,
        BVHNode nodes[],
        int iteration, curandState *states) {

    d_render<<< gridSize, blockSize, sharedMemSize >>>(
        d_output, colors, imageW, imageH, 
        primitives, numPrimitives,
        materials, numMaterials,
        nodes,
        iteration, 
        states);
}

void kernelSetInverseViewMatrix(float invViewMatrix[]) {
    cutilSafeCall(cudaMemcpyToSymbol(c_invViewMatrix, invViewMatrix, sizeof(float4)*3));
}

void kernelSetCameraInfo(CameraInfo &cameraInfo) {
    cutilSafeCall(cudaMemcpyToSymbol(c_cameraInfo, &cameraInfo, sizeof(CameraInfo)));
}

void kernelSetDebug(bool debug) {
    cutilSafeCall(cudaMemcpyToSymbol(c_debug, &debug, sizeof(bool)));
}

void kernelSetSceneBounds(float3 minB, float3 maxB) {
    cutilSafeCall(cudaMemcpyToSymbol(minBound, &minB, sizeof(float3)));
    cutilSafeCall(cudaMemcpyToSymbol(maxBound, &maxB, sizeof(float3)));
}


void kernelSetTextures(float4 primitives[], int primitivesSize,
                       float4 nodes[], int nodesSize) {
                       //float4 materials[], int materialsSize) {
        //float4 bih[], int bihSize) {    
    primitivesTex.normalized = false;
    primitivesTex.filterMode = cudaFilterModePoint;
    primitivesTex.addressMode[0] = cudaAddressModeWrap;
    cutilSafeCall(cudaBindTexture(0, primitivesTex, primitives, primitivesSize));

    nodesTex.normalized = false;
    nodesTex.filterMode = cudaFilterModePoint;
    nodesTex.addressMode[0] = cudaAddressModeWrap;
    cutilSafeCall(cudaBindTexture(0, nodesTex, nodes, nodesSize));

    /*
    materialsTex.normalized = false;
    materialsTex.filterMode = cudaFilterModePoint;
    materialsTex.addressMode[0] = cudaAddressModeWrap;
    cutilSafeCall(cudaBindTexture(0, materialsTex, materials, materialsSize));
    */

    /*bihTex.normalized = false;
    bihTex.filterMode = cudaFilterModePoint;
    bihTex.addressMode[0] = cudaAddressModeWrap;
    cutilSafeCall(cudaBindTexture(0, bihTex, bih, bihSize));*/
}


#endif
