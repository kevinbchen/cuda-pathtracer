#ifndef _BIH_H
#define _BIH_H

#include "cutil_math.h"
#include "primitives.h"
#include <vector>
#include <set>
#include <cstdio>
using namespace std;

struct BIHNode {
	enum {
		X = 0,
		Y = 1,
		Z = 2,
		LEAF = 3,
	};
	/*int type;*/
    int children[2]; // Note: lower 2 bits of children[0] indicate type
    float clip[2];

	/*union {
		struct {int objects[2];};
		struct {
			float clip[2]; 
			int left;
			int right;
		};
	};*/
};

struct BIHInfo {
    /*BIHInfo() {}
	BIHInfo(BIHNode *_node, float _tmin, float _tmax) : node(_node), tmin(_tmin), tmax(_tmax) {};*/
	int node;
	float tmin;
	float tmax;
};

class BIH
{
public:
    vector<BIHNode> nodes;
	BIHNode *rootNode;
	float3 minBound;
	float3 maxBound;
	vector<Primitive> *primitives;

	BIH(vector<Primitive> *primitives, float3 minb, float3 maxb);
	~BIH(void);

	float primitiveCenter(Primitive &p, int axis);
	float primitiveMin(Primitive &p, int axis);
	float primitiveMax(Primitive &p, int axis);

	int subdivide(int l, int r, float3 &minBound, float3 &maxBound, int axis, int depth);
	void build();
	bool getNearestIntersection(Ray &ray, Intersection &intersection);
	bool getOccluder(Ray &ray, float tmax);

	void getInfo(int index, int &numNodes, int &numLeafs, int &numEmpty, int &numPrimitives);
};

#endif
