#ifndef _BVH_H
#define _BVH_H

#include "cutil_math.h"
#include "primitives.h"
#include <vector>
#include <set>
#include <cstdio>
using namespace std;

struct BVHNode {
	enum {
		X = 0,
		Y = 1,
		Z = 2,
		LEAF = 3,
	};
	/*int type;*/
    int children[2]; // Note: lower 2 bits of children[0] indicate type
    float3 minB;
    float3 maxB;

	/*union {
		struct {int objects[2];};
		struct {
			float clip[2]; 
			int left;
			int right;
		};
	};*/
};

struct BVHInfo {
    /*BVHInfo() {}
	BVHInfo(BVHNode *_node, float _tmin, float _tmax) : node(_node), tmin(_tmin), tmax(_tmax) {};*/
	int node;
	float tmin;
	float tmax;
};

class BVH
{
public:
    vector<BVHNode> nodes;
	BVHNode *rootNode;
	float3 minBound;
	float3 maxBound;
	vector<Primitive> *primitives;

	BVH(vector<Primitive> *primitives, float3 minb, float3 maxb);
	~BVH(void);

	float primitiveCenter(Primitive &p, int axis);
	float primitiveMin(Primitive &p, int axis);
    float3 primitiveMin(Primitive &p);
	float primitiveMax(Primitive &p, int axis);
    float3 primitiveMax(Primitive &p);

	int subdivide(int l, int r, float3 &minBound, float3 &maxBound, int axis, int depth);
	void build();
	bool getNearestIntersection(Ray &ray, Intersection &intersection);
	bool getOccluder(Ray &ray, float tmax);

	void getInfo(int index, int &numNodes, int &numLeafs, int &numEmpty, int &numPrimitives, 
        int &maxPrimitivesPerLeaf);
};

#endif
