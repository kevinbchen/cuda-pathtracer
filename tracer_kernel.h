/*
 * Lab 6 - Volume Rendering Fractals
 */

#ifndef _TRACER_KERNEL_H_
#define _TRACER_KERNEL_H_

#include <curand_kernel.h>
#include "cutil_inline.h"
#include "cutil_math.h"
#include "math_constants.h"
#include "primitives.h"
#include "bih.h"
#include "bvh.h"
#include "scene.h"

typedef struct {
    float4 m[3];
} float3x4;

struct Material;
struct CameraInfo;

//__constant__ float3x4 c_invViewMatrix;  // inverse view matrix


extern "C" void kernelSetup(dim3 gridSize, dim3 blockSize, uint imageW, uint imageH, curandState *states);
extern "C" void kernelRender(dim3 gridSize, dim3 blockSize, uint sharedMemSize,
        uint *d_output, float3 *colors, 
        uint imageW, uint imageH, 
        Primitive primitives[], int numPrimitives,
        Material materials[], int numMaterials,
        BVHNode nodes[],
        int iteration, curandState *states);

extern "C" void kernelSetInverseViewMatrix(float invViewMatrix[]);
extern "C" void kernelSetCameraInfo(CameraInfo &cameraInfo);
extern "C" void kernelSetDebug(bool debug);
extern "C" void kernelSetSceneBounds(float3 minB, float3 maxB);
//extern "C" void kernelSetTextures(float4 primitives[], int primitivesSize);// float4 bih[], int bihSize);
extern "C" void kernelSetTextures(
        float4 primitives[], int primitivesSize,
        float4 nodes[], int nodesSize);
        //float4 materials[], int materialsSize);
#endif
