#ifndef _PRIMITIVES_H_
#define _PRIMITIVES_H_

struct Ray {
	float3 o;	// origin
	float3 d;	// direction
};

enum {TRIANGLE = 0, SPHERE = 1};
struct Primitive {
    int type;
    float3 p1;   // position
    float3 p2;   // position
    float3 p3;   // position
    float3 n1;
    float3 n2;
    float3 n3;
    //float  r;   // radius
    int m;        // material id
};

struct Intersection {
    float  t;   // parametric ray value
    //float3 p;   // point of intersection
    //float3 n;   // normal at intersection
};

#endif
