EXEC=tracer
OBJECTS=tracer.o scene.o bih.o bvh.o tracer_kernel.o

NVCC=nvcc
CC=g++
NVCCFLAGS=-gencode=arch=compute_10,code=\"sm_10,compute_10\" -m64 --compiler-options -fno-strict-aliasing -I. -I/usr/include -Icommon/inc -DUNIX -O2
LDFLAGS=-fPIC -m64 -L/usr/local/cuda/lib64 -Lcommon/lib/linux -lcudart -lGL -lGLU -lX11 -lGLEW_x86_64 -L/usr/X11R6/lib64 -lGLEW_x86_64 -L/usr/X11R6/lib64 -lglut -L/usr/lib64 -lcutil_x86_64

$(EXEC): $(OBJECTS)
	$(CC) -o $(EXEC) $(OBJECTS) $(LDFLAGS)

%.o: %.cu
	$(NVCC) $(NVCCFLAGS) -c $<

clean:
	rm -f *.o $(EXEC)

run: clean $(EXEC)
	optirun ./$(EXEC)

.PHONY: clean
