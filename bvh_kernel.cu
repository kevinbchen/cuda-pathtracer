#ifndef _BVH_KERNEL_CU_
#define _BVH_KERNEL_CU_

#include <curand_kernel.h>
#include "cutil_math.h"
#include "primitives.h"
#include "bvh.h"

__device__ int getNearestIntersection(
    Ray &ray, BVHNode nodes[], Primitive primitives[], float &t, Primitive &primitive, int &numTraversals) {
	/*float3 invDir;
	invDir.x = 1.0f/ray.d.x;
	invDir.y = 1.0f/ray.d.y;
	invDir.z = 1.0f/ray.d.z;*/

    int BVHStack[100];
	//stack<BIHInfo> BIHStack;
	//BIHStack.push(BIHInfo(rootNode,tmin,tmax));

    int primitiveIndex = -1;
	BVHNode node;
    float4 x = tex1Dfetch(nodesTex, 0);
    node.children[0] = *(int *)&x.x;
    node.children[1] = *(int *)&x.y;
    node.minB = make_float3(tex1Dfetch(nodesTex, 1));
    node.maxB = make_float3(tex1Dfetch(nodesTex, 2));

    int index = -1;
    //BVHStack[index] = 0;
    float3 invR = make_float3(1.0f) / ray.d;

	bool useStack = false; //don't use stack for first one
	while (true) {
		numTraversals++;
        if (useStack) {
		    if (index < 0) break;
            
            int nodeIndex = BVHStack[index];
            float4 x = tex1Dfetch(nodesTex, nodeIndex * 3);
            node.children[0] = *(int *)&x.x;
            node.children[1] = *(int *)&x.y;
            node.minB = make_float3(tex1Dfetch(nodesTex, nodeIndex * 3 + 1));
            node.maxB = make_float3(tex1Dfetch(nodesTex, nodeIndex * 3 + 2));

		    index--;//BIHStack.pop();
        }
        useStack = true;

        // compute intersection of ray with all six bbox planes
        float3 tbot = invR * (node.minB - ray.o);
        float3 ttop = invR * (node.maxB - ray.o);

        // re-order intersections to find smallest and largest on each axis
        float3 tmin3 = fminf(ttop, tbot);
        float3 tmax3 = fmaxf(ttop, tbot);

        // find the largest tmin and the smallest tmax
        float tmin = fmaxf(fmaxf(tmin3.x, tmin3.y), tmin3.z);
        float tmax = fminf(fminf(tmax3.x, tmax3.y), tmax3.z);

        if (tmax < tmin || tmin >= t) continue; // no hit (or we already found something closer)

        int axis = node.children[0] & 0x3;
		if (axis != BVHNode::LEAF) {
            // push children to stack
            float rayd = ((float *)&ray.d)[axis];
            int nearChild = (node.children[0] >> 2);
            int farChild = node.children[1];
			if (rayd < 0.0f) {
				int temp = nearChild;
				nearChild = farChild;
				farChild = temp;
			}
            
            if (farChild != -1) {
                BVHStack[++index] = farChild;
            }
            if (nearChild != -1) {
                useStack = false;
                int nodeIndex = nearChild;
                float4 x = tex1Dfetch(nodesTex, nodeIndex * 3);
                node.children[0] = *(int *)&x.x;
                node.children[1] = *(int *)&x.y;
                node.minB = make_float3(tex1Dfetch(nodesTex, nodeIndex * 3 + 1));
                node.maxB = make_float3(tex1Dfetch(nodesTex, nodeIndex * 3 + 2));

                // check if child is "behind" the ray (ignore if so)
                float rayo = ((float *)&ray.o)[axis];
                if (rayd >= 0.0f) {
                    if (rayo > ((float *)&node.maxB)[axis]) {
                        useStack = true;
                    }
                } else {
                    if (rayo < ((float *)&node.minB)[axis]) {
                        useStack = true;
                    }
                }
            }
		} else {
            Primitive tempPrimitive;
			for (int i = node.children[0] >> 2; i <= node.children[1]; i++) {
                float4 x = tex1Dfetch(primitivesTex, i * 7 + 0);
                tempPrimitive.type = *(int*)&x.x;
                tempPrimitive.m = *(int*)&x.y;
                tempPrimitive.p1 = make_float3(tex1Dfetch(primitivesTex, i * 7 + 1));
                tempPrimitive.p2 = make_float3(tex1Dfetch(primitivesTex, i * 7 + 2));
                tempPrimitive.p3 = make_float3(tex1Dfetch(primitivesTex, i * 7 + 3));
                
                float tempT = primitiveIntersect(ray, tempPrimitive);
                if (tempT > 0 && tempT < t) {
					t = tempT;
					primitiveIndex = i;
                    primitive = tempPrimitive;	
				}
			}
		}
	}
	return primitiveIndex;
}

#endif
