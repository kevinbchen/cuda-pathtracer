# Interactive CUDA Pathtracer 

This project is a pathtracer written in CUDA. It takes advantage of the GPU
by parallelizing tracing rays per pixel in order to achieve semi-realtime
performance. It was written as my final project for CS 179: GPU Programming.

## Features
* physically based global illumination through Monte Carlo integration
* supports sphere and triangle primitives, as well as .obj and .ply models
* supports normal interpolation for triangles
* uses a custom `.scene` file format to easily load scenes
* supports diffuse, glossy, specular, and refractive BRDFs
* simple depth of field with a square aperture (dynamically adjustable)
* uses either a BIH (Bounding Interval Hierarchy) or a BVH (Bounding Volume
  Hierarchy) to spatially partition the scene
* basic camera interactivity

The best part is that it runs at reasonably interactive framerates!
(tested on a Thinkpad with a Nvidia Quadro K2000M graphics card)

## Example renders
![render1](renders/1.png)
![render2](renders/2.png)
![render3](renders/3.png)

Note that `i/s` is iterations per second, where an iteration consists of a 
single Monte Carlo path for each pixel in the image. The image converges as 
the number of iteration increases.

Here is a video demo of the interactivity:
[https://www.youtube.com/watch?v=MrF1XkneaAc](https://www.youtube.com/watch?v=MrF1XkneaAc)

## Running
To compile and run the program:

    > make
    > ./tracer data/test.scene

There are additional tests scenes included in `data/`. These are:

* `test.scene` - default test scene with cup, utah teapot, stanford bunny
* `test2.scene` - old benchmarking scene with stanford bunny and spheres
* `cornellbox.scene` - cornell box with refractive and reflective spheres

The program accepts a few controls:

* `1`, `2` - decrease/increase camera focal length
* `3`, `4` - decrease/increase camera aperture size
* `i` - toggle info text
* `d` - toggle BVH debug visualization

## Optimizations
In order to run at reasonable speeds, there were a lot of optimizations that 
were made (and some attempts at optimization that didn't pan out). See 
[OPTIMIZATIONS.md](OPTIMIZATIONS.md) for an in-depth rundown.