#include "scene.h"

Scene::Scene() {
    minBound = make_float3(10000, 10000, 10000);
    maxBound = make_float3(-10000, -10000, -10000);

    cameraInfo.position = make_float3(0, 1.5, 6);
    cameraInfo.focalLength = 5;
    cameraInfo.apertureSize = 0.05;
}

void Scene::build() {
    /*bih = new BIH(&primitives, minBound, maxBound);
	bih->build();*/

    bvh = new BVH(&primitives, minBound, maxBound);
	bvh->build();
    /*Ray ray;
    ray.o = make_float3(0,0,-5);
    ray.d = normalize(make_float3(0,0.1f,1));
    Intersection in;
    in.t = 1234;
    bih->getNearestIntersection(ray, in);
    printf("%f\n", in.t);*/
}

void Scene::addTriangle(float3 p1, float3 p2, float3 p3, 
        float3 n1, float3 n2, float3 n3,
        int materialId) {
    Primitive p;
    p.type = TRIANGLE;
    p.p1 = p1;
    p.p2 = p2;
    p.p3 = p3;
    p.n1 = n1;
    p.n2 = n2;
    p.n3 = n3;
    p.m = materialId;
    primitives.push_back(p);

    minBound = fminf(minBound, p1 - make_float3(1e-5));
    maxBound = fmaxf(maxBound, p1 + make_float3(1e-5));
    minBound = fminf(minBound, p2 - make_float3(1e-5));
    maxBound = fmaxf(maxBound, p2 + make_float3(1e-5));
    minBound = fminf(minBound, p3 - make_float3(1e-5));
    maxBound = fmaxf(maxBound, p3 + make_float3(1e-5));
}

void Scene::addSphere(float3 p, float r, int materialId) {
    Primitive s;
    s.type = SPHERE;
    s.p1 = p;
    s.p2.x = r;
    s.m = materialId;
    primitives.push_back(s);

    minBound = fminf(minBound, p - make_float3(r));
    maxBound = fmaxf(maxBound, p + make_float3(r));
}

void Scene::addModel(const char* filename, int materialId,
        float3 translation, float scale, bool smoothNormals) {
    printf("Loading %s...", filename);
    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        printf("file does not exist\n");
        return;
    }

    // Load vertex and face data.
    vector<float3> vertices;
    vector<int3> faces;
    if (strstr(filename, "ply")) {
        loadPly(file, vertices, faces);
    } else if (strstr(filename, "obj")) {
        loadObj(file, vertices, faces);
    } else {
        printf("unrecognized format\n");
        return;
    }
    fclose(file);

    // Apply vertex scaling/translation.    
    for (int i = 0; i < vertices.size(); i++) {
        vertices[i] = vertices[i] * scale + translation;
    }
    
    // Calculate face normals.
    vector<float3> faceNormals(faces.size());
    for (int i = 0; i < faces.size(); i++) {
        int3 v = faces[i];
        float3 edge1 = vertices[v.y] - vertices[v.x];
        float3 edge2 = vertices[v.z] - vertices[v.x];
        faceNormals[i] = normalize(cross(edge1, edge2));
    }

    // Calculate per-vertex normals.
    vector<float3> vertexNormals;
    vector<int> facesPerVertex;
    if (smoothNormals) {
        for (int i = 0; i < vertices.size(); i++) {
            vertexNormals.push_back(make_float3(0,0,0));
            facesPerVertex.push_back(0);
        }
        for (int i = 0; i < faces.size(); i++) {
            int3 v = faces[i];
            for (int j = 0; j < 3; j++) {
                int vi = ((int *) &v)[j];
                vertexNormals[vi] += faceNormals[i];;
                facesPerVertex[vi]++;
            }            
        }
        for (int i = 0; i < vertices.size(); i++) {
            if (facesPerVertex[i] != 0) {
                vertexNormals[i] = normalize(vertexNormals[i] / facesPerVertex[i]);
            } else {
                vertexNormals[i] = make_float3(0, 1, 0);
            }
        }
    }

    // Add triangles to scene.
    for (int i = 0; i < faces.size(); i++) {
        int3 v = faces[i];
        if (smoothNormals) {
            addTriangle(vertices[v.x], vertices[v.y], vertices[v.z], 
                vertexNormals[v.x], vertexNormals[v.y], vertexNormals[v.z],
                materialId);
        } else {
            addTriangle(vertices[v.x], vertices[v.y], vertices[v.z], 
                faceNormals[i], faceNormals[i], faceNormals[i],
                materialId);
        }
    }

    printf("Done:\n");
	printf("  Vertices: %d\n  Faces: %d\n\n", vertices.size(), faces.size());
}

void Scene::loadObj(FILE *file, vector<float3> &vertices, vector<int3> &faces) {
    int numFaces = 0;
    char line[256];
    while (fgets(line, sizeof(line), file)) {
        if (line[0] == 'v' && line[1] == ' ') {
            float3 v;
            sscanf(line, "%*s %f %f %f", &v.x, &v.y, &v.z);
            vertices.push_back(v);
        } else if (line[0] == 'v' && line[1] == 'n') {
            float3 n;
            sscanf(line, "%*s %f %f %f", &n.x, &n.y, &n.z);
            //normals.push_back(n);
        } else if (line[0] == 'f') {
            int3 v;
            char s[3][128];
            sscanf(line, "%*s %s %s %s", s[0], s[1], s[2]);
            for (int i = 0; i < 3; i++) {
                int j = 0;
                int n = strlen(s[i]);
                int start = 0;
                while (start < n) {
                    int end = strcspn(s[i] + start, "/");
                    if (j == 0) {
                        int vi;
                        sscanf(s[i] + start, "%d", &vi);
                        ((int *) &v)[i] = vi - 1;
                    }
                    start += end + 1;
                    j++;
                }
            }
            //if (numFaces < 200) {
            faces.push_back(v);
            numFaces++;
            //}
        }
    }
}


void Scene::loadPly(FILE *file, vector<float3> &vertices, vector<int3> &faces) {
    int numVerts = 0;
    int numFaces = 0;
    bool readingVerts = false;
    char line[256];
    while (fgets(line, sizeof(line), file)) {
        if (strstr(line, "element vertex")) {
            sscanf(line, "%*s %*s %d", &numVerts);
        } else if (strstr(line, "element face")) {
            sscanf(line, "%*s %*s %d", &numFaces);
        } else if (strstr(line, "end_header")) {
            readingVerts = true;
        } else if (readingVerts) {
            if (vertices.size() < numVerts) {
                float3 v;
                sscanf(line, "%f %f %f", &v.x, &v.y, &v.z);
                vertices.push_back(v);
            } else {
                int3 v;
                sscanf(line, "%*d %d %d %d", &v.x, &v.y, &v.z);
                faces.push_back(v);
            }
        }
    }
}

void Scene::loadScene(const char *filename) {
    char path[256] = "";
    const char *slash = strrchr(filename, '/');
    if (slash) {
        int n = (slash + 1) - filename;
        strncpy(path, filename, n);
        path[n] = '\0';
    }
    
    printf("Loading scene %s...", filename);
    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        printf("file does not exist\n");
        return;
    }
    printf("Done\n");

    map<string, int> materialIds;

    char line[256];
    while (fgets(line, sizeof(line), file)) {
        if (startsWith(line, "cam")) {
            sscanf(line, "%*s (%f,%f,%f) %f %f", 
                &cameraInfo.position.x, &cameraInfo.position.y, &cameraInfo.position.z,
                &cameraInfo.apertureSize, &cameraInfo.focalLength);
        } else if (startsWith(line, "mat")) {
            Material m;
            char name[128];
            sscanf(line, "%*s %s (%f,%f,%f) (%f,%f,%f) %f %f", name,
                &m.color.x, &m.color.y, &m.color.z,
                &m.emission.x, &m.emission.y, &m.emission.z,
                &m.reflect, &m.refract);
            materials.push_back(m);
            materialIds[string(name)] = materials.size() - 1;
        } else if (startsWith(line, "sphere")) {
            float3 p;
            float r;
            char mStr[128];
            sscanf(line, "%*s (%f,%f,%f) %f %s", &p.x, &p.y, &p.z, &r, mStr);
            int mId = materialIds[string(mStr)];
            addSphere(p, r, mId); 
        } else if (startsWith(line, "tri")) {
            float3 p1, p2, p3;
            char mStr[128];
            sscanf(line, "%*s (%f,%f,%f) (%f,%f,%f) (%f,%f,%f) %s", 
                &p1.x, &p1.y, &p1.z, &p2.x, &p2.y, &p2.z, &p3.x, &p3.y, &p3.z, mStr);
            float3 edge1 = p2 - p1;
            float3 edge2 = p3 - p1;
            float3 n = normalize(cross(edge1, edge2));
            int mId = materialIds[string(mStr)];
            addTriangle(p1, p2, p3, n, n, n, mId);
        } else if (startsWith(line, "model")) {
            float3 t;
            float scale;
            char fn[128], mStr[128];
            int smooth;
            sscanf(line, "%*s %s (%f,%f,%f) %f %d %s", fn,
                &t.x, &t.y, &t.z, &scale, &smooth, mStr);
            int mId = materialIds[string(mStr)];
            char fullpath[128];
            strcpy(fullpath, path);
            strcat(fullpath, fn);
            addModel(fullpath, mId, t, scale, smooth);
        }
    }
}

bool Scene::startsWith(const char *str, const char *prefix) {
    return strncmp(str, prefix, strlen(prefix)) == 0;
}

void Scene::setupKernelScene() {
    int numPrimitives = primitives.size();
    float4 *tempPrimitives = new float4[numPrimitives * 7];
    for (int i = 0; i < numPrimitives; i++) {
        Primitive p = primitives[i];
        tempPrimitives[i*7 + 0] = make_float4(
                *(float*) &p.type, *(float*) &p.m, 0, 0);
        tempPrimitives[i*7 + 1] = make_float4(p.p1);
        tempPrimitives[i*7 + 2] = make_float4(p.p2); 
        tempPrimitives[i*7 + 3] = make_float4(p.p3);
        tempPrimitives[i*7 + 4] = make_float4(p.n1);
        tempPrimitives[i*7 + 5] = make_float4(p.n2);
        tempPrimitives[i*7 + 6] = make_float4(p.n3);
    }

    int primitivesTexSize = numPrimitives * 7 * sizeof(float4);
    cutilSafeCall(cudaMalloc(&d_primitivesTex, primitivesTexSize));
    cutilSafeCall(cudaMemcpy(d_primitivesTex, tempPrimitives, primitivesTexSize,
            cudaMemcpyHostToDevice));
    delete[] tempPrimitives;


    int numNodes = bvh->nodes.size();
    float4 *tempNodes = new float4[numNodes * 3];
    for (int i = 0; i < numNodes; i++) {
        BVHNode &node = bvh->nodes[i];
        tempNodes[i * 3] = make_float4(
            *(float*) &node.children[0],
            *(float*) &node.children[1],
            0, 0);
        tempNodes[i * 3 + 1] = make_float4(node.minB, 0);
        tempNodes[i * 3 + 2] = make_float4(node.maxB, 0);
    }

    int nodesTexSize = numNodes * 3 * sizeof(float4);
    cutilSafeCall(cudaMalloc(&d_nodesTex, nodesTexSize));
    cutilSafeCall(cudaMemcpy(d_nodesTex, tempNodes, nodesTexSize,
            cudaMemcpyHostToDevice));
    delete[] tempNodes;

    /*int numNodes = bih->nodes.size();
    float4 *tempNodes = new float4[numNodes];
    for (int i = 0; i < numNodes; i++) {
        BIHNode node = bih->nodes[i];
        tempNodes[i] = make_float4(
            *((float*) &node.children[0]),
            *((float*) &node.children[1]),
            node.clip[0], node.clip[1]);
        //tempNodes[i * 3 + 1] = make_float4(node.minB, 0);
        //tempNodes[i * 3 + 2] = make_float4(node.maxB, 0);
    }

    int nodesTexSize = numNodes * sizeof(float4);*/

    /*int numMaterials = materials.size();
    float4 *temp = new float4[numPrimitives * 9];
    for (int i = 0; i < numPrimitives; i++) {
        Primitive p = primitives[i];
        temp[i*9 + 0] = make_float4(*(float*) &p.type, p.reflect, p.refract, 0);
        temp[i*9 + 1] = make_float4(p.p1);
        temp[i*9 + 2] = make_float4(p.p2); 
        temp[i*9 + 3] = make_float4(p.p3);
        temp[i*9 + 4] = make_float4(p.n1);
        temp[i*9 + 5] = make_float4(p.n2);
        temp[i*9 + 6] = make_float4(p.n3);
        temp[i*9 + 7] = make_float4(p.c);
        temp[i*9 + 8] = make_float4(p.e);        
    }

    int primitivesTexSize = numPrimitives * 9 * sizeof(float4);
    cutilSafeCall(cudaMalloc(&d_primitivesTex, primitivesTexSize));
    cutilSafeCall(cudaMemcpy(d_primitivesTex, temp, primitivesTexSize,
            cudaMemcpyHostToDevice));
    delete[] temp;*/

    kernelSetTextures(d_primitivesTex, primitivesTexSize, d_nodesTex, nodesTexSize);
    kernelSetSceneBounds(minBound, maxBound);
}
