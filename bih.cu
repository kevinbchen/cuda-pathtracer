#include "bih.h"
//minBound = minb;
//maxBound = maxb;

BIH::BIH(vector<Primitive> *primitives, float3 minb, float3 maxb) {
	this->primitives = primitives;
	minBound = minb;
	maxBound = maxb;
}

BIH::~BIH(void) {
}


float BIH::primitiveCenter(Primitive &p, int axis) {
	if (p.type == TRIANGLE) {
        return ((float *) &((p.p1 + p.p2 + p.p3)/3.0f))[axis];
	} else if (p.type == SPHERE) {
        return ((float *) &p.p1)[axis];
	}
}

float BIH::primitiveMax(Primitive &p, int axis) {
	if (p.type == TRIANGLE) {
        return ((float *) &fmaxf(fmaxf(p.p1, p.p2), p.p3))[axis];
	} else if (p.type == SPHERE) {
        return ((float *) &(p.p1 + make_float3(p.p2.x)))[axis];
	}
}

float BIH::primitiveMin(Primitive &p, int axis) {
	if (p.type == TRIANGLE) {
        return ((float *) &fminf(fminf(p.p1, p.p2), p.p3))[axis];       
    } else if (p.type == SPHERE) {
        return ((float *) &(p.p1 - make_float3(p.p2.x)))[axis];
	}
}


int BIH::subdivide(int l, int r, float3 &minBound, float3 &maxBound, int axis, int depth) {
    if (r < l) {
        printf("no nodes\n");
        return -1;
    }
	float3 diff = maxBound-minBound;
	axis = (axis + 1) % 3;/*BIHNode::Z;
	if (diff.x > diff.y) {
		if (diff.x > diff.z) {
			axis = BIHNode::X;
		}
	}
	else {
		if (diff.y > diff.z) {
			axis = BIHNode::Y;
		}
	}*/

	if (r-l < 1 || depth >= 50) {// || diff.x + diff.y + diff.z <= 0.1) { //leaf node;
        
        //if (depth >= 30) printf("depth %d %d %d\n", depth, r-l);
		//printf ("leaf (%d %d): ",l,r);
		BIHNode node;
		//node.type = BIHNode::LEAF;
		node.children[0] = (l << 2) | BIHNode::LEAF;
		node.children[1] = r;
        nodes.push_back(node);
		/*for (int i=l; i<=r; i++) {
			node->objects.push_back((*primitives)[i]);
			//cout << "object ";
		}*/

		//cout << "\n";
		return nodes.size() - 1;
	}
	else {
		BIHNode node;
		//node.type = axis;
		node.children[0] = ((-1) << 2) | axis;
		node.children[1] = -1;
        nodes.push_back(node);
        int nodeIndex = nodes.size()-1;
		//node->clip[0] = 0;
		//node->clip[1] = 0;



        float minB = ((float *) &minBound)[axis];
        float maxB = ((float *) &maxBound)[axis];



        /*int nPrims = r - l + 1;
		float* eleft = new float[nPrims];
		float* eright = new float[nPrims];

		set<float> splitSet;
        //printf("axis %d\n", axis);
		for (int i = 0; i < nPrims; i++) {
			eleft[i] = primitiveMin((*primitives)[l+i], axis);
			eright[i] = primitiveMax((*primitives)[l+i], axis);
			splitSet.insert(eleft[i]);
			splitSet.insert(eright[i]);
            //printf("%f ", eleft[i]);
		}
        //printf("\n");

		bool* pleft = new bool[nPrims];
		int* nleft = new int[splitSet.size()];
		int* nright = new int[splitSet.size()];
        for (int i = 0; i < splitSet.size(); i++) {
            nleft[i] = 0;
            nright[i] = 0;
        }


		int i = 0;
		for(set<float>::iterator iter = splitSet.begin(); iter != splitSet.end(); iter++) {
			for (int j=0; j<nPrims; j++) {
				if (eleft[j] < *iter) {
					nleft[i]++;
				}
				if (eright[j] >= *iter) {
					nright[i]++;
				}
			}
			i++;
		}

		float SAV = 0.5f / (diff.x*diff.z + diff.x*diff.y + diff.z*diff.y);
		// calculate cost for not splitting
		float cLeaf = nPrims * 1.0f;

		float minCost = 1000000;
		float bestPos = -1.0f;

		i = 0;
		for(set<float>::iterator iter = splitSet.begin(); iter != splitSet.end(); iter++) {
			float3 left = diff;
            ((float *) &left)[axis] = *iter - minB;

			float3 right = diff;
			((float *) &right)[axis] = maxB - *iter;

			float SA1 = 2 * (left.x*left.z + left.x*left.y + left.z*left.y);
			float SA2 = 2 * (right.x*right.z + right.x*right.y + right.z*right.y);
			float cost = 0.3f + 1.0f * (SA1 * SAV * nleft[i] + SA2 * SAV * nright[i]);
			if (cost < minCost) {
                //printf("cost %f %f %f %f %d %d \n", cost, SA1, SA2, SAV, nleft[i], nright[i]);
				minCost = cost;
				bestPos = *iter;
			}
            i++;
		}

        float plane = bestPos; //(minB+maxB)*0.5f;
        //printf("%d %f\n", axis, bestPos);
        delete[] eleft;
        delete[] eright;
		delete[] pleft;
		delete[] nleft;
		delete[] nright;*/

        int i = l;
		int j = r;

        float plane = (minB+maxB) * 0.5f;

		float lclip = minB;
		float rclip = maxB;
		while (i < j) {
			while (primitiveCenter((*primitives)[i],axis) <= plane) {
				float temp = primitiveMax((*primitives)[i],axis);
				if (temp > lclip) {
					lclip = temp;
				}
				i++;
				if (i > r) break;
			}
			while (primitiveCenter((*primitives)[j],axis) > plane) {
				float temp = primitiveMin((*primitives)[j],axis);
				if (temp < rclip) {
					rclip = temp;
				}
				j--;
				if (j < l) break;
			}
			if (i < j) {
				std::swap((*primitives)[i],(*primitives)[j]);
				/*Primitive *p = primitives[i];
				primitives[i] = primitives[j];
				primitives[j] = p;*/
				//i++;
				//j--;
			}
		}

		int newAxis = axis;
		/*switch (axis)
		{
			case BIHNode::X : newAxis = BIHNode::Y; break;
			case BIHNode::Y : newAxis = BIHNode::Z; break;
			case BIHNode::Z : newAxis = BIHNode::X; break;
		}*/


		if (lclip > minB) {
			node.clip[0] = lclip;
			float3 newMaxBound = maxBound;
            ((float *) &newMaxBound)[axis] = lclip;
			node.children[0] = (subdivide(l,j,minBound,newMaxBound,newAxis,depth+1) << 2) | axis;
		}
		if (rclip < maxB) {
			node.clip[1] = rclip;
			float3 newMinBound = minBound;
            ((float *) &newMinBound)[axis] = rclip;
			node.children[1] = subdivide(i,r,newMinBound,maxBound,newAxis,depth+1);
		}
        nodes[nodeIndex] = node;
		//printf("\n depth: %d axis: %d  clip: %f %f",depth,axis,node->clip[0],node->clip[1]);
        return nodeIndex;
	}
}


void BIH::build() {
	printf("Building BIH tree...");

	float3 diff = maxBound-minBound;
	int axis = BIHNode::Z;
	if (diff.x > diff.y) {
		if (diff.x > diff.z) {
			axis = BIHNode::X;
		}
	}
	else {
		if (diff.y > diff.z) {
			axis = BIHNode::Y;
		}
	}

	//rootNode = subdivide(0,primitives->size()-1,minBound,maxBound,axis,0);
    subdivide(0,primitives->size()-1,minBound,maxBound,axis,0);

	printf("Done:\n");

	int numNodes = 0;
	int numLeafs = 0;
	int numEmpty = 0;
	int numPrimitives = 0;
	getInfo(0,numNodes,numLeafs,numEmpty,numPrimitives);
	printf("  Total Nodes: %d\n  Empty Nodes: %d\n  Leaf Nodes: %d\n  Primitives: %d\n\n",
        numNodes,numEmpty,numLeafs,numPrimitives);

}
//
//bool BIH::getNearestIntersection(Ray &ray, Intersection &in) {
//	/*float3 invDir;
//	invDir.x = 1.0f/ray.d.x;
//	invDir.y = 1.0f/ray.d.y;
//	invDir.z = 1.0f/ray.d.z;*/
//
//	float tmin = 0.0f;
//	float tmax = 100000.0f;
//
//	bool outside = false;
//    if (ray.o.x > maxBound.x || ray.o.x < minBound.x ||
//        ray.o.y > maxBound.y || ray.o.y < minBound.y ||
//        ray.o.z > maxBound.z || ray.o.z < minBound.z) {
//            outside = true;
//    }
//	
//	if (outside)
//	{
//		//tmin = (maxBound.z-ray.p.z)*invDir.z;//ray.d.z; //z is flipped :|
//		//tmax = (minBound.z-ray.p.z)*invDir.z;///ray.d.z;
//
//		//Vector3 rmin = ray.p + tmin*ray.d;
//		//Vector3 rmax = ray.p + tmax*ray.d;
//
//		for (int i=0; i<3; i++){
//            float rayo, rayd, minB, maxB;
//            if (i == 0) {
//                rayo = ray.o.x;
//                rayd = ray.d.x;
//                minB = minBound.x;
//                maxB = maxBound.x;
//            } else if (i == 1) {
//                rayo = ray.o.y;
//                rayd = ray.d.y;
//                minB = minBound.y;
//                maxB = maxBound.y;
//            } else if (i == 2) {
//                rayo = ray.o.z;
//                rayd = ray.d.z;
//                minB = minBound.z;
//                maxB = maxBound.z;
//            }
//
//			float minPos = rayo + tmin*rayd;
//			float maxPos = rayo + tmax*rayd;
//			if (rayd < 0) {
//				if (maxPos > maxB || minPos < minB) {
//					return false;
//				}
//
//				if (maxPos < minB) {
//					tmax = tmin + (minB-minPos)/(rayd);
//				}
//				if (minPos > maxB) {
//					tmin = (maxB-minPos)/(rayd);
//				}
//			}
//			else {
//				if (maxPos < minB || minPos > maxB) {
//					return false;
//				}
//
//				if (maxPos > maxB) {
//					tmax = tmin + (maxB-minPos)/(rayd);
//				}
//				if (minPos < minB) {
//					tmin = (minB-minPos)/(rayd);
//				}
//			}
//			if (tmin >= tmax) return false;
//		}
//
//
//		/*if (minBound.x > rmin.x) {
//			tmin = (minBound.x-ray.p.x)*invDir.x;///ray.d.x;
//		}
//		else if (maxBound.x < rmin.x) {
//			tmin = (maxBound.x-ray.p.x)*invDir.x;///ray.d.x;
//		}
//		if (minBound.x > rmax.x) {
//			tmax = (minBound.x-ray.p.x)*invDir.x;///ray.d.x;
//		}
//		else if (maxBound.x < rmax.x) {
//			tmax = (maxBound.x-ray.p.x)*invDir.x;///ray.d.x;
//		}
//
//		if (minBound.y > rmin.y) {
//			tmin = (minBound.y-ray.p.y)*invDir.y;///ray.d.y;
//		}
//		else if (maxBound.y < rmin.y) {
//			tmin = (maxBound.y-ray.p.y)*invDir.y;///ray.d.y;
//		}
//		if (minBound.y > rmax.y) {
//			tmax = (minBound.y-ray.p.y)*invDir.y;///ray.d.y;
//		}
//		else if (maxBound.y < rmax.y) {
//			tmax = (maxBound.y-ray.p.y)*invDir.y;///ray.d.y;
//		}*/
//
//		if (tmax <= tmin) return false;
//	}
//
//
//	/*stack<BIHNode*> nodes;
//	stack<float> tmins;
//
//	BIHNode* node = rootNode;
//
//	while (tmin < 10000.0f) {
//		while (node && node->type != BIHNode::LEAF) {
//			int axis = node->type;
//			float leftPlane = (node->clip[0]-ray.p.cell[axis])/ray.d.cell[axis];
//			float rightPlane = (node->clip[1]-ray.p.cell[axis])/ray.d.cell[axis];
//
//			BIHNode *nearNode = NULL;
//			BIHNode *farNode = NULL;
//			float nearPlane, farPlane;
//
//			if (ray.d.cell[axis] < 0.0f) {
//				nearPlane = rightPlane;
//				farPlane = leftPlane;
//				nearNode = node->right;
//				farNode = node->left;
//			}
//			else {
//				nearPlane = leftPlane;
//				farPlane = rightPlane;
//				nearNode = node->left;
//				farNode = node->right;
//			}
//
//			if (farPlane > tmax) {
//				if (nearPlane > tmin) {
//					tmax = min(tmax,nearPlane);
//					node = nearNode;
//				}
//				else {
//					if (nodes.empty()) {
//						return false;
//					}
//					else {
//						node = nodes.top();
//						nodes.pop();
//
//						tmin = tmins.top();
//						tmins.pop();
//					}
//				}
//			}
//			else if (nearPlane < tmin) {
//				tmin = max(tmin,farPlane);
//				node = farNode;
//			}
//			else {
//				tmin = min(tmin,farPlane);
//				nodes.push(farNode);
//				tmins.push(tmin);
//				node = nearNode;
//			}
//		}
//
//		if (node) {
//			return true;
//			float distance = -1.0f;
//			Primitive* primitive = NULL;
//
//			intersection.t = -1;
//			for (int i=0; i<node->objects.size(); i++) {
//				Primitive* p = node->objects[i];
//				Intersection tempIntersection;
//				if (p->intersect(ray,tempIntersection)) {
//					if (intersection.t == -1 || tempIntersection.t < intersection.t) {
//						intersection = tempIntersection;
//						primitive = p;
//					}
//				}
//			}
//			if (primitive != NULL) {
//				return true;
//			}
//		}
//
//		if (nodes.empty()) {
//			return false;
//		}
//		else {
//			node = nodes.top();
//			nodes.pop();
//
//			tmin = tmins.top();
//			tmins.pop();
//		}
//	}
//
//	return false;*/
//
//	int numTraversals = 0;
//	int numPrimitives = 0;
//
//	float distance = tmax;
//	in.t = tmax;
//
//	stack<BIHInfo> BIHStack;
//	//BIHStack.push(BIHInfo(rootNode,tmin,tmax));
//
//    Primitive* primitive = NULL;
//	BIHNode *node = &nodes[0];
//	bool useStack = false; //don't use stack for first one
//	while (true) {
//		numTraversals++;
//		if (useStack) {
//			if (BIHStack.empty()) break;
//			node = BIHStack.top().node;
//			tmin = BIHStack.top().tmin;
//			tmax = BIHStack.top().tmax;
//			BIHStack.pop();
//		}
//		useStack = true;
//		if (distance < tmin) continue;
//
//		//if (node == NULL) continue;
//		if (node->type == BIHNode::LEAF) {
//			//return true;			
//
//			//intersection.t = -1;
//			//for (int i=0; i<node->objects.size(); i++) {
//			for (int i=node->objects[0]; i<=node->objects[1]; i++) {
//				numPrimitives++;
//				Primitive p = (*primitives)[i];//node->objects[i];
//				Intersection tempIntersection;
//                if (primitiveIntersect(ray, p, tempIntersection)) {
//					if (tempIntersection.t < in.t) {
//						in = tempIntersection;
//						primitive = &(*primitives)[i];
//						distance = in.t;
//					}
//				}
//			}
//			if (primitive != NULL) {
//				//return true;
//			}
//		}
//		else {
//			int axis = node->type;
//            float rayo, rayd, minB, maxB, invDir;
//            if (axis == 0) {
//                rayo = ray.o.x;
//                rayd = ray.d.x;
//                minB = minBound.x;
//                maxB = maxBound.x;
//            } else if (axis == 1) {
//                rayo = ray.o.y;
//                rayd = ray.d.y;
//                minB = minBound.y;
//                maxB = maxBound.y;
//            } else if (axis == 2) {
//                rayo = ray.o.z;
//                rayd = ray.d.z;
//                minB = minBound.z;
//                maxB = maxBound.z;
//            }
//            invDir = 1.0f / rayd;
//
//			float nearPlane = (node->clip[0]-rayo)*invDir;///ray.d.cell[axis];
//			float farPlane = (node->clip[1]-rayo)*invDir;///ray.d.cell[axis];
//
//			BIHNode *nearNode = &nodes[node->left];
//			BIHNode *farNode = &nodes[node->right];
//
//			if (rayd < 0.0f) {
//				float tempPlane = nearPlane;
//				nearPlane = farPlane;
//				farPlane = tempPlane;
//
//				BIHNode *tempNode = nearNode;
//				nearNode = farNode;
//				farNode = tempNode;
//			}
//
//			if (tmin >= nearPlane && tmax <= farPlane) { // empty
//				//continue;
//			}
//			else if (tmin >= nearPlane) { // far node only
//				if (farNode != NULL) {
//					//tmin = max(tmin,farPlane);
//					//BIHStack.push(BIHInfo(farNode,max(tmin,farPlane),tmax));
//					node = farNode;
//					tmin = max(tmin,farPlane);
//					useStack = false;
//					//continue;
//				}
//			}
//			else if (tmax <= farPlane) { // near node only
//				if (nearNode != NULL) {
//					//tmax = min(tmax,nearPlane);
//					//BIHStack.push(BIHInfo(nearNode,tmin,min(tmax,nearPlane)));
//					node = nearNode;
//					tmax = min(tmax,nearPlane);
//					useStack = false;
//				}
//			}
//			else { // near to far (both)
//				if (farNode != NULL) {
//					if (nearNode != NULL) {
//						BIHStack.push(BIHInfo(farNode,farPlane,tmax));
//					}
//					else {
//						useStack = false;
//						node = farNode;
//						tmin = farPlane;
//					}
//				}
//				if (nearNode != NULL) {
//					//BIHStack.push(BIHInfo(nearNode,tmin,nearPlane));
//					useStack = false;
//					node = nearNode;
//					tmax = nearPlane;
//				}
//			}
//		}
//	}
//
//	//in.numTraversals = numTraversals;
//	//in.numPrimitives = numPrimitives;
//	return primitive != NULL;
//}


/*bool BIH::getOccluder(Ray &ray, float tmax) {
	float3 invDir;
	invDir.x = 1.0f/ray.d.x;
	invDir.y = 1.0f/ray.d.y;
	invDir.z = 1.0f/ray.d.z;

	float tmin = 0.0f;
	//float tmax = 100000.0f;

	stack<BIHInfo> BIHStack;
	//BIHStack.push(BIHInfo(rootNode,tmin,tmax));

	BIHNode *node = rootNode;
	bool useStack = false; //don't use stack for first one
	while (true) {
		if (useStack) {
			if (BIHStack.empty()) break;
			node = BIHStack.top().node;
			tmin = BIHStack.top().tmin;
			tmax = BIHStack.top().tmax;
			BIHStack.pop();
		}
		useStack = true;

		//if (node == NULL) continue;
		if (node->type == BIHNode::LEAF) {
			//for (int i=0; i<node->objects.size(); i++) {
			for (int i=node->objects[0]; i<node->objects[1]; i++) {
				Primitive* p = (*primitives)[i];//node->objects[i];
				Intersection tempIntersection;
				if (p->intersect(ray,tempIntersection)) {
					if (tempIntersection.t <= tmax) {
						return true;
					}
					else {
						//return false;
					}
				}
			}
		}
		else {
			int axis = node->type;
			float nearPlane = (node->clip[0]-ray.o.cell[axis])*invDir.cell[axis];///ray.d.cell[axis];
			float farPlane = (node->clip[1]-ray.o.cell[axis])*invDir.cell[axis];///ray.d.cell[axis];

			BIHNode *nearNode = node->left;
			BIHNode *farNode = node->right;

			if (ray.d.cell[axis] < 0.0f) {
				float tempPlane = nearPlane;
				nearPlane = farPlane;
				farPlane = tempPlane;

				BIHNode *tempNode = nearNode;
				nearNode = farNode;
				farNode = tempNode;
			}

			if (tmin >= nearPlane && tmax <= farPlane) { // empty
				//continue;
			}
			else if (tmin >= nearPlane) { // far node only
				if (farNode != NULL) {
					//tmin = max(tmin,farPlane);
					//BIHStack.push(BIHInfo(farNode,max(tmin,farPlane),tmax));
					node = farNode;
					tmin = max(tmin,farPlane);
					useStack = false;
					//continue;
				}
			}
			else if (tmax <= farPlane) { // near node only
				if (nearNode != NULL) {
					//tmax = min(tmax,nearPlane);
					//BIHStack.push(BIHInfo(nearNode,tmin,min(tmax,nearPlane)));
					node = nearNode;
					tmax = min(tmax,nearPlane);
					useStack = false;
				}
			}
			else { // near to far (both)
				if (farNode != NULL) {
					if (nearNode != NULL) {
						BIHStack.push(BIHInfo(farNode,farPlane,tmax));
					}
					else {
						useStack = false;
						node = farNode;
						tmin = farPlane;
					}
				}
				if (nearNode != NULL) {
					//BIHStack.push(BIHInfo(nearNode,tmin,nearPlane));
					useStack = false;
					node = nearNode;
					tmax = nearPlane;
				}
			}
		}
	}
	return false;
}
*/

void BIH::getInfo(int index, int &numNodes, int &numLeafs, int &numEmpty, int &numPrimitives) {
	if (index == -1) {
		numEmpty++;
	} else if ((nodes[index].children[0] & 0x3) == BIHNode::LEAF) {
		numPrimitives += nodes[index].children[1] - (nodes[index].children[0] >> 2) + 1;
		numLeafs++;
	} else {
		numNodes++;
		getInfo(nodes[index].children[0] >> 2,numNodes,numLeafs,numEmpty,numPrimitives);
		getInfo(nodes[index].children[1],numNodes,numLeafs,numEmpty,numPrimitives);
	}

}
