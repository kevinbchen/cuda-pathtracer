/*
* CUDA Pathtracer
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include <GL/glew.h>

#if defined (__APPLE__) || defined(MACOSX)
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <cuda_gl_interop.h>
#include <curand_kernel.h>
#include "cutil_inline.h"
#include "tracer_kernel.h"
#include "scene.h"

typedef unsigned int uint;
typedef unsigned char uchar;


/* Screen drawing kernel parameters */
uint width = 512, height = 512;
dim3 blockSize(8, 8);
dim3 gridSize(width / blockSize.x, height / blockSize.y);

/* View settings */
float3 viewRotation = make_float3(0.5, 0.5, 0.0);
float3 viewTranslation = make_float3(0.0, -1.5, -6.0f);
float invViewMatrix[12];

// OpenGL buffer pixel buffer
GLuint pbo = 0;


curandState *d_states;

Scene scene;

int iteration = 1;
bool showInfo = true;
bool debug = false;
float3 *d_colors;
Primitive *d_primitives;
int numPrimitives;
Material *d_materials;
int numMaterials;
BVHNode *d_nodes;
double startTime;

void initPixelBuffer();

// return the current time, in seconds
double now() {
    return (double) clock() / CLOCKS_PER_SEC;
}

void clear() {
    cutilSafeCall(cudaMemset(d_colors, 0, width*height*sizeof(float3)));
    iteration = 1;
    startTime = now();
}

void setup() {
    viewTranslation = -scene.cameraInfo.position;

    numPrimitives = scene.primitives.size();//sizeof(primitives)/sizeof(Primitive);
    numMaterials = scene.materials.size();

    /*int numNodes = scene.bvh->nodes.size();
    float4 *bih = new float4[numNodes * 3];
    for (int i = 0; i < numNodes; i++) {
        BVHNode &node = scene.bvh->nodes[i];
        bih[i * 3] = make_float4(
            *(float*) &node.children[0],
            *(float*) &node.children[1],
            0, 0);
        bih[i * 3 + 1] = make_float4(node.minB, 0);
        bih[i * 3 + 1] = make_float4(node.maxB, 0);
    }

    float4 *d_bihTex;
    cutilSafeCall(cudaMalloc(&d_bihTex, scene.bih->nodes.size() * sizeof(float4)));*/

    int numNodes = scene.bvh->nodes.size();
    cutilSafeCall(cudaMalloc(&d_colors, width*height*sizeof(float3)));
    cutilSafeCall(cudaMalloc(&d_primitives, numPrimitives * sizeof(Primitive)));
    cutilSafeCall(cudaMalloc(&d_materials, numMaterials * sizeof(Material)));
    cutilSafeCall(cudaMalloc(&d_nodes, numNodes * sizeof(BVHNode)));
    cutilSafeCall(cudaMalloc(&d_states, width*height*sizeof(curandState)));

    cutilSafeCall(cudaMemcpy(d_primitives, &scene.primitives[0], 
                numPrimitives * sizeof(Primitive),
                cudaMemcpyHostToDevice));
    cutilSafeCall(cudaMemcpy(d_materials, &scene.materials[0], 
                numMaterials * sizeof(Material),
                cudaMemcpyHostToDevice));
    cutilSafeCall(cudaMemcpy(d_nodes, &scene.bvh->nodes[0], numNodes * sizeof(BVHNode),
            cudaMemcpyHostToDevice));

    /*cutilSafeCall(cudaMemcpy(d_bihTex, bih, scene.bih->nodes.size() * sizeof(float4),
            cudaMemcpyHostToDevice));*/

    scene.setupKernelScene();
    kernelSetup(gridSize, blockSize, width, height, d_states);

    cutilCheckMsg("setup kernel failed");
    clear();
}

/* Execute rendering kernel */
void render() {
    // set necessary constants in hardware
    kernelSetInverseViewMatrix(invViewMatrix);

    // map PBO to get CUDA device pointer
    uint *d_output;
    cutilSafeCall(cudaGLMapBufferObject((void**)&d_output, pbo));
    cutilSafeCall(cudaMemset(d_output, 0, width*height*4));

    // call CUDA kernel, writing results to PBO
    unsigned int sharedMemSize(numMaterials * sizeof(Material));
    kernelRender(gridSize, blockSize, sharedMemSize,
            d_output, d_colors, width, height, 
            d_primitives, numPrimitives,
            d_materials, numMaterials,
            d_nodes,
            iteration, 
            d_states);

    cutilCheckMsg("render kernel failed");
    cutilSafeCall(cudaGLUnmapBufferObject(pbo));
}

// display results using OpenGL (called by GLUT)
void display() {
    // use OpenGL to build view matrix
    GLfloat modelView[16];
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
        glLoadIdentity();
        glRotatef(-viewRotation.y, 0.0, 1.0, 0.0);
        glRotatef(-viewRotation.x, 1.0, 0.0, 0.0);
        glTranslatef(-viewTranslation.x, -viewTranslation.y, -viewTranslation.z);
    glGetFloatv(GL_MODELVIEW_MATRIX, modelView);
    glPopMatrix();

    // transpose matrix to conform with OpenGL's notation
    invViewMatrix[0] = modelView[0]; invViewMatrix[1] = modelView[4]; invViewMatrix[2] = modelView[8]; invViewMatrix[3] = modelView[12];
    invViewMatrix[4] = modelView[1]; invViewMatrix[5] = modelView[5]; invViewMatrix[6] = modelView[9]; invViewMatrix[7] = modelView[13];
    invViewMatrix[8] = modelView[2]; invViewMatrix[9] = modelView[6]; invViewMatrix[10] = modelView[10]; invViewMatrix[11] = modelView[14];

    // do render
    render();

    // display results
    glClear(GL_COLOR_BUFFER_BIT);

    // draw image from PBO
    glDisable(GL_DEPTH_TEST);
    glRasterPos2i(0, 0);
    glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, pbo);
    glDrawPixels(width, height, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, 0);

    if (showInfo) {
        // print text
        char buffer[100];
        double t = now() - startTime;
        sprintf(buffer, "Iteration: %-6d Time: %-6.1f i/s: %.1f", 
                iteration, t, iteration/t);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        gluOrtho2D(0, width, 0, height);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glViewport(0, 0, width, height);

        for (int i = 0; i < strlen(buffer); i++) {
            glColor3f(0, 0, 0);
            glRasterPos3f(10 + 9*i + 1, height - 20 - 1, 0);
            glutBitmapCharacter(GLUT_BITMAP_9_BY_15, buffer[i]);
            glColor3f(1, 1, 1);
            glRasterPos3f(10 + 9*i, height - 20, 0);
            glutBitmapCharacter(GLUT_BITMAP_9_BY_15, buffer[i]);
        }
    }

    iteration += 1;

    glutSwapBuffers();
    glutReportErrors();
    glutPostRedisplay();
}

void idle() {
}

void setConsts() {
    kernelSetCameraInfo(scene.cameraInfo);
    kernelSetDebug(debug);
}

void keyboard(unsigned char key, int x, int y) {
    bool reset = false;

    switch(key) {
        case 27:
            exit(0);
            break;
        case '1':
            reset = true;
            scene.cameraInfo.focalLength -= 0.5;
            printf("camera focal length: %f\n", scene.cameraInfo.focalLength);
            break;
        case '2':
            reset = true;
            scene.cameraInfo.focalLength += 0.5;
            printf("camera focal length: %f\n", scene.cameraInfo.focalLength);
            break;
        case '3':
            reset = true;
            scene.cameraInfo.apertureSize *= 0.8;
            printf("camera aperture size: %f\n", scene.cameraInfo.apertureSize);
            break;
        case '4':
            reset = true;
            scene.cameraInfo.apertureSize *= 1.2;
            printf("camera aperture size: %f\n", scene.cameraInfo.apertureSize);
            break;
        case 'i':
            showInfo = !showInfo;
            break;
        case 'd':
            reset = true;
            debug = !debug;
            break;
        default:
            break;
    }

    if (reset) {
        setConsts();
        clear();
    }
    glutPostRedisplay();
}

int ox, oy;
int buttonState = 0;

void mouse(int button, int state, int x, int y) {
    if (state == GLUT_DOWN) {
        buttonState |= 1<<button;
    } else if (state == GLUT_UP) {
        buttonState = 0;
    }

    ox = x; oy = y;
    glutPostRedisplay();
}

void motion(int x, int y) {
    float dx, dy;
    dx = x - ox;
    dy = y - oy;

    if (buttonState & 4) {
        // left+middle = zoom
        viewTranslation.z += dy / 100.0;
    } 
    else if (buttonState & 2) {
        // middle = translate
        viewTranslation.x += dx / 100.0;
        viewTranslation.y -= dy / 100.0;
    }
    else if (buttonState & 1) {
        // left = rotate
        viewRotation.x += dy / 5.0;
        viewRotation.y += dx / 5.0;
    }

    ox = x; oy = y;
    clear();
    glutPostRedisplay();
}

void reshape(int x, int y) {
    width = x; height = y;
    // reinitialize with new size
    initPixelBuffer();

    glViewport(0, 0, x, y);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0, 1.0, 0.0, 1.0, 0.0, 1.0);

    clear();
    glutPostRedisplay();
}

void cleanup() {
    cutilSafeCall(cudaFree(d_colors));
    cutilSafeCall(cudaFree(d_primitives));
    cutilSafeCall(cudaFree(d_materials));
    cutilSafeCall(cudaFree(d_nodes));
    cutilSafeCall(cudaFree(d_states));
    cutilSafeCall(cudaGLUnregisterBufferObject(pbo));    

    glDeleteBuffersARB(1, &pbo);
    cudaDeviceReset();
}

int iDivUp(int a, int b){
    return (a % b != 0) ? (a / b + 1) : (a / b);
}

void initPixelBuffer() {
    if (pbo) {
        // delete old buffer
        cutilSafeCall(cudaGLUnregisterBufferObject(pbo));
        glDeleteBuffersARB(1, &pbo);
    }

    // create pixel buffer object for display
    glGenBuffersARB(1, &pbo);
    glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, pbo);
    glBufferDataARB(GL_PIXEL_UNPACK_BUFFER_ARB, width*height*sizeof(GLubyte)*4, 0, GL_STREAM_DRAW_ARB);
    glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, 0);
    
    cutilSafeCall(cudaGLRegisterBufferObject(pbo));

    // calculate new grid size
    gridSize = dim3(iDivUp(width, blockSize.x), iDivUp(height, blockSize.y));
}

////////////////////////////////////////////////////////////////////////////////
// Program main
////////////////////////////////////////////////////////////////////////////////
int main(int argc, char** argv) {
    if (argc <= 1) {
        printf("usage: %s scenefile\n", argv[0]);
        exit(0);
    }

    // initialize CUDA
    cudaSetDevice(cutGetMaxGflopsDeviceId());    
    int device;
    struct cudaDeviceProp prop;
    cudaGetDevice(&device);
    cudaGetDeviceProperties(&prop, device);

    // initialize GLUT callback functions
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
    glutInitWindowSize(width, height);
    glutCreateWindow("CUDA pathtracer");
    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutMouseFunc(mouse);
    glutMotionFunc(motion);
    glutReshapeFunc(reshape);
    glutIdleFunc(idle);
    glewInit();
    if (!glewIsSupported("GL_VERSION_2_0 GL_ARB_pixel_buffer_object")) {
        fprintf(stderr, "Required OpenGL extensions missing.");
        exit(-1);
    }
    initPixelBuffer();
    atexit(cleanup);    

    // load scene and setup kernel
    scene.loadScene(argv[1]);
    scene.build();

    setup();
    setConsts();
    clear();

    // print keys
    printf("Keys:\n");
    printf("  1, 2 - decrease/increase camera focal length\n");
    printf("  3, 4 - decrease/increase camera aperture size\n");
    printf("  i - toggle info text\n");
    printf("  d - toggle BVH debug visualization\n");
    printf("\n");
    glutMainLoop();

    cleanup();
    return 0;
}
