#ifndef _SCENE_H_
#define _SCENE_H_

#include <curand_kernel.h>
#include <vector>
#include <map>
#include <string>
#include "tracer_kernel.h"
#include "cutil_math.h"
#include "primitives.h"
#include "bih.h"
#include "bvh.h"
using namespace std;

struct Material {
    float3 color;
    float3 emission;
    float reflect;
    float refract;
};

struct CameraInfo {
    float3 position;
    float focalLength;   // focal length
    float apertureSize;  // aperture size
};

class Scene {
public:
    vector<Primitive> primitives;
    vector<Material> materials;

    BIH *bih;
    BVH *bvh;
	float3 minBound;
	float3 maxBound;

    float4 *d_primitivesTex;
    float4 *d_materialsTex;
    float4 *d_nodesTex;

    CameraInfo cameraInfo;

    Scene();
    ~Scene() {};
    

    void addTriangle(float3 p1, float3 p2, float3 p3, 
            float3 n1, float3 n2, float3 n3,
            int materialId);

    void addSphere(float3 p, float r, int materialId);

    void addModel(const char* filename, int materialId,
            float3 translation, float scale, bool smoothNormals = false);
    void loadObj(FILE *file, vector<float3> &vertices, vector<int3> &faces);
    void loadPly(FILE *file, vector<float3> &vertices, vector<int3> &faces);

    void loadScene(const char* filename);

    void build();

    void setupKernelScene();
private:

    bool startsWith(const char *str, const char *prefix);


};
#endif
